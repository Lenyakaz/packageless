/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.dao;

import com.mycompany.packagelessshop.dto.ProductDTO;
import com.mycompany.packagelessshop.entity.CartEntity;
import com.mycompany.packagelessshop.entity.CartEntity.Builder;
import com.mycompany.packagelessshop.entity.ProductEntity;
import com.mycompany.packagelessshop.entity.ShoppingEntity;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 *
 * @author tbiro
 */
@Singleton
@NoArgsConstructor //Lombok project
@AllArgsConstructor //ez  is
public class CartDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    public void newCart(ProductDTO productDTO,ShoppingEntity whichShopping,ProductEntity whichProduct){
        System.out.println("Jártam a DAO-ban!");
        Builder builder = new Builder();
        builder.setProduct(whichProduct);
        builder.setShopping(whichShopping);
        builder.setWeight(productDTO.getCalcWeight());
        em.persist(builder.build());
    }
}
