/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.dao;

import javax.persistence.TypedQuery;
import com.mycompany.packagelessshop.entity.User;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.eclipse.persistence.internal.oxm.mappings.Login;

/**
 *
 * @author Otto
 */
public class LoginDAO {
    
    @PersistenceContext
	private EntityManager entityManager;

	public Login login(String email, String password) {
		Query q = entityManager.createQuery("select l from Login l where l.email=:em and l.password=:pa");
		q.setParameter("em", email);
		q.setParameter("pa", password);
		return (Login) q.getSingleResult();
	} 
    
    
    
}
     
    
   

