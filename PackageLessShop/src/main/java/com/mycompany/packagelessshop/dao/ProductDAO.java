
package com.mycompany.packagelessshop.dao;

import com.mycompany.packagelessshop.entity.ProductEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**getProductByIdWithPlainQuery(id);
 *getProductByNameWithTypedQuery(name)
 * @author tbiro
 */
@Singleton
@NoArgsConstructor //Lombok project
@AllArgsConstructor //ez  is
public class ProductDAO {
    private static String QUERY_FIND_ALL="select b from ProductEntity b";
    // private static String QUERY_FIND_UNDERLIMIT="Select p From ProductEntity p WHERE p.stock < 30000 ORDER BY p.name,p.stock ASC"; 
    private static String QUERY_FIND_UNDERLIMIT="Select p From ProductEntity p ORDER BY p.name "; 
      
    
    
    @PersistenceContext
    private EntityManager em;
    
    //Gergő kérte, hogy namedQuery-ket kell használnunk!
    public List<ProductEntity> findAll(){
        return em.createQuery(QUERY_FIND_ALL).getResultList();
    }
    
    public List<ProductEntity> findProductsUnderLimit(){
        return em.createQuery(QUERY_FIND_UNDERLIMIT).getResultList();
    }
    
     public List<ProductEntity> findProductsUnderNewLimit(long quantity,String ordertype){
        // return em.createQuery("Select p From ProductEntity p WHERE p.stock >?1" ).getResultList();
         System.out.println("ordertype:"+ ordertype);
        return em.createQuery("Select p From ProductEntity p WHERE p.stock <'"+ quantity + "'" + (ordertype)).getResultList();
         
    }
     
                 
    
    
    public ProductEntity getProductByIdWithPlainQuery(Long id) { //hagyományos query, visszad egy entity-t id alapján
            long idMax =(long)em.createQuery("Select max(i.id) From ProductEntity i").getSingleResult();
            if (id<1 || id>idMax){
                return null;
            }
            else {
            Query jpqlQuery = em.createQuery("SELECT p FROM ProductEntity p WHERE p.id=:id");
	    jpqlQuery.setParameter("id", id);
	    return (ProductEntity) jpqlQuery.getSingleResult();
            }
    }

    public List<ProductEntity> getProductByNameWithTypedQuery(String string) { //typedQuery: Itt a visszatérő típus biztos Product
	    TypedQuery<ProductEntity> typedQuery
	      = em.createQuery("SELECT u FROM ProductEntity u WHERE u.name like :string", ProductEntity.class);
	    typedQuery.setParameter("string", "%"+string+"%");
            System.out.println(typedQuery.getResultList());
	    return typedQuery.getResultList();
	}
    public void minusStock(ProductEntity productById, int weightMinus){ 
        //Ez fogja módosítani az adott productId Stock értékét weightMinus-szal!
        productById.setStock(productById.getStock()-weightMinus);
        em.persist(productById);
        System.out.println("Minuszoltam a készletet!");
    }
}