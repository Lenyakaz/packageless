package com.mycompany.packagelessshop.dao;

import com.mycompany.packagelessshop.entity.User;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class UserDAO {

    @PersistenceContext
    private EntityManager em;

    public User getUser(String username,String password) {
        System.out.println(username);
        System.out.println(password);
        
        List<User> users = em.createQuery("SELECT u From User u Where u.username = :username ", User.class)
        // List<User> users = em.createQuery("SELECT u From User u Where u.username = :username AND u.password = : password ", User.class)
                
                .setParameter("username", username)
               // .setParameter("password",password)
                .getResultList();
                
        System.out.println("users size: " + users.size());
        if (users.size() == 0) {
            return null;
        }
        return users.get(0);
    }
}
