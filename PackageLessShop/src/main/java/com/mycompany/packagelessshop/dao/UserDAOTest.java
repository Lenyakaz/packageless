
package com.mycompany.packagelessshop.dao;


import com.mycompany.packagelessshop.entity.User;
import com.mycompany.packagelessshop.entity.UserEntity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class UserDAOTest {

    
    public void CreateUserEntity (String username,String email,String password) {
       
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("packagelessshop");
        EntityManager entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        
        User newUser = new User(username,email,password);
        UserEntity userentity = new UserEntity ( username,email,password); 
                
                
        entityManager.persist(newUser);
        entityManager.persist(userentity);
        
        
        entityManager.getTransaction().commit();
        entityManager.close();
        factory.close();
    }
}
    
    
    

