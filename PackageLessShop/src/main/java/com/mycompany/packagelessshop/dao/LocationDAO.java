/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.dao;

import com.mycompany.packagelessshop.entity.LocationEntity;
import com.mycompany.packagelessshop.entity.ProductEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 *
 * @author tbiro
 */
@Singleton
@NoArgsConstructor //Lombok project
@AllArgsConstructor //ez  is
public class LocationDAO {
    private static final String QUERY_FIND_ALL = "Select l From LocationEntity l ";
    private static final String QUERY_FIND_WANNASHOP_IS_BIGGER1 ="Select l From LocationEntity l WHERE l.wannaShop > 0 ORDER BY l.shoppingDate,l.shoppingTime,l.wannaShop"; 
    private static final String QUERY_FIND_BY_DISTRICT="Select d From LocationEntity Where District d.district=:dist";
    //"SELECT p FROM ProductEntity p WHERE p.id=:id"
    //SELECT c.name FROM Country c WHERE c.population > 1000000 ORDER BY c.name
    //"Select l From LocationEntity l "
    //Select l.wannaShop From LocationEntity l WHERE l.wannaShop > 0 ";
    //Select l.address, l.district, l.shoppingDate , l.shoppingTime, l.wannaShop From LocationEntity l
    
    
    @PersistenceContext
    private EntityManager em;
    
    public List<LocationEntity> findAll(){
        System.out.println("Eddig eéóljut  program?");
       return em.createQuery(QUERY_FIND_ALL).getResultList(); //majd kell rendezni dátum, idő alapján!
     
}
    
    public List<LocationEntity> findAllEntity(){
       return em.createQuery(QUERY_FIND_ALL).getResultList(); //majd kell rendezni dátum, idő alapján!
     
}
    
    
    public List<LocationEntity> findWannaShopIsBiggerThan(){
       return em.createQuery(QUERY_FIND_WANNASHOP_IS_BIGGER1).getResultList();
     
}
    public LocationEntity findByDistrict (int dist){
        System.out.println("locationDAO lekérdezés előtt");
        Query jpqlQuery = em.createQuery("Select d From LocationEntity Where d.district=:dist");
//        Query jpqlQuery = em.createQuery("SELECT p FROM ProductEntity p WHERE p.id=:id");//ez volt a productDAO-ban
	    jpqlQuery.setParameter("dist", dist);
            System.out.println("lefut a lekérdezés");
	    return (LocationEntity) jpqlQuery.getSingleResult();
       
    }
    
    
}
