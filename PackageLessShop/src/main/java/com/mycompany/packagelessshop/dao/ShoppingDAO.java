
package com.mycompany.packagelessshop.dao;

import com.mycompany.packagelessshop.entity.LocationEntity;
import com.mycompany.packagelessshop.entity.ShoppingEntity;
import com.mycompany.packagelessshop.entity.ShoppingEntity_reserve;
import com.mycompany.packagelessshop.entity.User;
import com.mycompany.packagelessshop.entity.UserEntity;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Singleton
@NoArgsConstructor //Lombok project
@AllArgsConstructor //Lombok
public class ShoppingDAO {

    private static String QUERY_FIND_ALL="select s from ShoppingEntity_reserve s ORDER BY s.localDate";
    private static String QUERY_FIND_EXACT_DATE ="select s from ShoppingEntity_reserve s WHERE s.localDate = :pDate";
    private static String QUERY_FIND_OVERLIMIT ="select s from ShoppingEntity_reserve s WHERE s.moneySpent > 5000 ORDER by s.moneySpent";
    
    
    @PersistenceContext
    private EntityManager em;

    public ShoppingEntity newShopping(int moneySpent,LocationEntity location,UserEntity user) {
        System.out.println("Jártam a DAO-ban!");
        ShoppingEntity newShopping = new ShoppingEntity();
        newShopping.setDateAndTime(LocalDateTime.now());
        newShopping.setMoneySpent(moneySpent);
        newShopping.setLocation(location);
        newShopping.setUser(user);
        em.persist(newShopping);
        return newShopping;
    }
//-------------------- otto cuccai ------------------------------------
    
    
    public List<ShoppingEntity_reserve> findAll(){
        System.out.println("find All - DAOban vagyok");
        return em.createQuery(QUERY_FIND_ALL).getResultList();
    }
    
    public List<ShoppingEntity_reserve> findDateOfShopping(Date date){
        System.out.println("find date of soppingban vok - DAOban vagyok");
        return em.createQuery(QUERY_FIND_EXACT_DATE).setParameter("pDate", date.toInstant()
	      .atZone(ZoneId.systemDefault())
	      .toLocalDate()).getResultList();
    }
    
   /* public List<XXXEntity> findAll(){                               // shopping reserve entitinek tarcsi
        return em.createQuery(QUERY_FIND_ALL).getResultList();           ha nem futna a másikkal 
                                                                         innen lehetne duplikálni az entitit ?!
    }*/
    
    
    public List<ShoppingEntity_reserve> findSalesOverLimit(){
        return em.createQuery(QUERY_FIND_OVERLIMIT).getResultList();
    }
    
    public List<ShoppingEntity_reserve> ShoppingEntityByName(String string) { 
	    TypedQuery<ShoppingEntity_reserve> typedQuery
	      = em.createQuery("SELECT s FROM ShoppingEntity_reserve s WHERE s.localDate like : string", ShoppingEntity_reserve.class);
	    typedQuery.setParameter("string", "%"+string+"%");
            System.out.println(typedQuery.getResultList());
	    return typedQuery.getResultList();
	}
    
    
    
    
    
}

