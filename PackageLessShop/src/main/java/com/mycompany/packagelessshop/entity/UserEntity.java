package com.mycompany.packagelessshop.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "user")
public class UserEntity extends BaseEntity {   
    
    public UserEntity(long id) {
        super.setId(id);
    }

    public UserEntity(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
    
    @Column 
    private String username;
    
    @Column
    private String email;

    @Column
    private String password;

    @OneToMany(mappedBy = "user") 
    private List<ShoppingEntity> shoppings;
}
