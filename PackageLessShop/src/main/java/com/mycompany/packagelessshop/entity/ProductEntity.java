package com.mycompany.packagelessshop.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table (name = "product")
public class ProductEntity extends BaseEntity {
    
    @Column
    private String name;

    @Column
    private String description;

    @Column
    private double price;

    @Column
    private int stock;   

    @OneToMany(mappedBy = "product") 
    private List<CartEntity> carts;

     
    @Override
    public String toString() {
        return "ProductEntity{" + "id=" + super.getId() + ", name=" + name + ", description=" + description + ", price=" + price + ", stock=" + stock + '}';
    }
  
}
