/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.entity;

import com.mycompany.packagelessshop.dto.ProductDTO;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author tbiro
 */
@Getter
@Setter
@Entity
@Table(name = "cart")
public class CartEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "shopping_id")
    private ShoppingEntity shopping;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private ProductEntity product;

    int weight;
     
    private CartEntity(Builder builder) {
        this.shopping=builder.shopping;
        this.product=builder.product;
        this.weight=builder.weight;
        }
    
    private CartEntity(){
    }
    
    @Setter
    public static class Builder {
        
        private ShoppingEntity shopping;
        private ProductEntity product;
        int weight;
        
        public CartEntity build() {
            System.out.println("Jártam a Builder-ben!");
            return new CartEntity(this);
        }
 
    }
}
