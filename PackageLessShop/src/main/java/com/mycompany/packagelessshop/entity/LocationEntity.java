package com.mycompany.packagelessshop.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "location")
public class LocationEntity extends BaseEntity {

    public LocationEntity(long id) {
        super.setId(id);
    }

    public LocationEntity() {
    }
    
    
    
    @Column
    private int district;

    @Column
    private String address;

    @Column
    private LocalDate shoppingDate;

    @Column
    private LocalTime shoppingTime;

    @Column
    private long wannaShop;
    
    @OneToMany(mappedBy = "location") //kapcsolat a ShoppingEntity táblával
    private List<ShoppingEntity> shoppings;

}


