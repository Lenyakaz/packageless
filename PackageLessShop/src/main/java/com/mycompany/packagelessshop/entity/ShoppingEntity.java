/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.entity;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author tbiro
 */
@Getter
@Setter
@Entity
@Table (name = "shopping")
public class ShoppingEntity extends BaseEntity {

    @ManyToOne //(fetch=FetchType.LAZY) //majd meglátjuk kell-e a FetchType!
    @JoinColumn (name="location_id") //itt megadjuk, hogy a locationEntity location_id néven csatlakozik manyToOne kapcsolattal
    private LocationEntity location;
    
    @ManyToOne
    @JoinColumn (name="user_id")
    private UserEntity user;
    
    @Column(name="date_time")
    private LocalDateTime dateAndTime;
    
    int moneySpent; //ez kijönne a product adatbázis price*weight egyenletből, de feltétezem, hogy megváltozhat az ár,
                       //így a későbbi problémák elkerülése miatt tettem ide
    
    @OneToMany(mappedBy = "shopping") 
    private List<CartEntity> carts;
}


