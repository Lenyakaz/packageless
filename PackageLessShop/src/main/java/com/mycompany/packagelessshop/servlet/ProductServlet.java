/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.servlet;

import com.mycompany.packagelessshop.dto.ProductDTO;
import com.mycompany.packagelessshop.entity.ProductEntity;
import com.mycompany.packagelessshop.filterbean.ListProductBean;
import com.mycompany.packagelessshop.mapper.ProductMapper;
import com.mycompany.packagelessshop.service.ProductService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tbiro
 */
@WebServlet(name = "ProductServlet", urlPatterns = {"/ProductServlet"})
public class ProductServlet extends HttpServlet {

    @EJB
    private ProductService productService;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) //ez mikor indul el?
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) //ez az alap, ez indul el.
            throws ServletException, IOException {
        
        String getString = req.getParameter("string");

        if (getString == null || getString.isEmpty()) {
              List<ProductDTO> productList = productService.findAll(); //ez most kiszedi egy metódussal az összeset, beleteszi egy listába, melyet felhasznál a jsp
//            ListProductBean listProductBean = new ListProductBean(); //ez csak egy kamu beiktatott osztály, mert így oldottuk meg órán
//            listProductBean.setProducts(ProductMapper.INSTANCE.toDTOList(productList)); //ez a mapper valójában mit csinál?
            //listProductBean=egy dto listát tartalmazó osztály
            //setProduct= ez állítja be a listát (egy setter)
            //(ami zárójelben van)=az a mappelés entity-ből dto-vá. Majd visszadja az eredményt
            
            req.setAttribute("productList", productList);// ez tárolja a teljes product listát a jsp oldal számára

        } else {
            List<ProductDTO> productList = productService.findByName(getString);
            req.setAttribute("productList", productList);
        }
        req.getRequestDispatcher("products.jsp").forward(req, res); //átirányít a weboldalra, ahol
        //ki tudjuk íratni a productList tartalmát!
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
