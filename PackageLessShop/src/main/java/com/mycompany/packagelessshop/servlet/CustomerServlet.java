/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.servlet;

import com.mycompany.packagelessshop.entity.LocationEntity;
import com.mycompany.packagelessshop.entity.ShoppingEntity;
import com.mycompany.packagelessshop.entity.User;
import com.mycompany.packagelessshop.entity.UserEntity;
import com.mycompany.packagelessshop.filterbean.ListProductBean;
import com.mycompany.packagelessshop.service.ProductService;
import com.mycompany.packagelessshop.service.ShoppingService;
import java.io.IOException;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tbiro
 */
@WebServlet(name = "CustomerServlet", urlPatterns = {"/CustomerServlet"})
public class CustomerServlet extends HttpServlet {

    @EJB
    ProductService productService;
    @EJB
    ShoppingService shoppingService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("finalize.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        ListProductBean listProductBean = (ListProductBean) session.getAttribute("listProductBean");
        String isCartDeleted = request.getParameter("toDelete");
        if (isCartDeleted == null || isCartDeleted.isEmpty()) {
            setupShoppingAndCart(request); //ez megcsinálja a bevásároló listához hozzáadást, készlet minuszolást
        } else {
            emptyCart(listProductBean, session);
            System.out.println("Töröltem a be.listát vásárlás nélkül!");
        }
        request.getRequestDispatcher("thanks.jsp").forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void setupShoppingAndCart(HttpServletRequest request) {
        HttpSession session = request.getSession();
        ListProductBean listProductBean = (ListProductBean) session.getAttribute("listProductBean");
        int moneySpent = listProductBean.getTotalMoneyinCart();
        long randomUser = 1; //most csak egy van nekem, majd Session-ból ki kell szedni ki van bejelentkezve
        UserEntity user = new UserEntity(randomUser);
        long randomLocation = (long) (Math.random() * 22 + 1); //most csak egy van nekem, majd Session-ból ki kell szedni ki van bejelentkezve
        LocationEntity location = new LocationEntity(randomLocation);
        ShoppingEntity forShoppingId = shoppingService.setShopping(moneySpent, location, user);
        shoppingService.setCart(listProductBean.getProducts(), forShoppingId); //ebben történik a készlet minuszolása is 
        emptyCart(listProductBean, session);
    }

    public void emptyCart(ListProductBean listProductBean, HttpSession session) {
        //bevásárló lista törlése
        listProductBean.setTotalMoneyinCart(0);
        listProductBean.setProducts(new ArrayList<>());
        session.setAttribute("listProductBean", listProductBean);
        session.setAttribute("numberOfProductInCart", 0);
        session.setAttribute("totalMoneyinCart", 0);
    }
}
