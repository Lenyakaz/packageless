
package com.mycompany.packagelessshop.servlet;

import com.mycompany.packagelessshop.entity.LocationEntity;
import com.mycompany.packagelessshop.service.LocationService;
import com.mycompany.packagelessshop.util.UpdateLocations;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tbiro
 */
@WebServlet(name = "LocationServlet", urlPatterns = {"/LocationServlet"})
public class LocationServlet extends HttpServlet {

    
    @EJB
    private LocationService locationService;

    @EJB
    private UpdateLocations updatelocations;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<LocationEntity> locationList = locationService.findAllEntity(); //ez majd a LocationDTO-ból dolgozik, de most csak próba
        System.out.println("Do-Get");
        System.out.println(locationList);
        request.setAttribute("locationList", locationList);//ez tárolja a teljes plocation listát a jsp oldal számára
        request.getRequestDispatcher("locations.jsp").forward(request, response); 
        
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
                
        long district = Integer.parseInt(request.getParameter("district"));
        long quantity = Integer.parseInt(request.getParameter("quantity")); 
        
        
        PrintWriter pw = response.getWriter();

        pw.println("a regisztrál vásárlók száma : ");
        pw.println(quantity);
        pw.println("a regisztrált kerület  : ");
        pw.println(district);
               
        UpdateLocations updatelocations = new UpdateLocations (); 
        updatelocations.LocationUpdate(district, quantity);
        
        List<LocationEntity> locationList = locationService.findAllEntity(); //ez majd a LocationDTO-ból dolgozik, de most csak próba
        //System.out.println(locationList);
        request.setAttribute("locationList", locationList);//ez tárolja a teljes plocation listát a jsp oldal számára
        request.getRequestDispatcher("/index.jsp").forward(request, response); 
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
