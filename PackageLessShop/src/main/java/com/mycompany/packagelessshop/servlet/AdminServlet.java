
package com.mycompany.packagelessshop.servlet;

import com.mycompany.packagelessshop.dto.ProductDTO;
import com.mycompany.packagelessshop.service.ProductService;
import com.mycompany.packagelessshop.util.Gmailer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AdminServlet", urlPatterns = {"/AdminServlet"})
public class AdminServlet extends HttpServlet {

    @EJB
    private ProductService productService;
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        

        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String button = request.getParameter("button");

        if ("button1".equals(button)) {

            List<ProductDTO> productNeedLoad = productService.findProductsUnderLimit();
            System.out.println("productNeedLoad");
            System.out.println(productNeedLoad);
            request.setAttribute("productNeedLoad", productNeedLoad);
            request.getRequestDispatcher("admin.jsp").forward(request, response);

            System.out.println("kifogyó-1es gomb lett megnyomva");
            
        } else if ("button2".equals(button)) {
            System.out.println("reg+jogos");
            request.getRequestDispatcher("index.jsp").forward(request, response);
            
        } else if ("button3".equals(button)) {
            System.out.println("napi bevétel és fogy");
            request.getRequestDispatcher("/StatisticServlet").forward(request, response);
            
            
            
            
        } else if ("button4".equals(button)) {
            System.out.println("ez lesz a mailes");

            //Gmailer gmailer = new Gmailer();
            //gmailer.GmailSendSSL();
            request.getRequestDispatcher("admin.jsp").forward(request, response);

        } else {            // levalogatós limit alapján 

           String ordertype ="" ; 
           long quantity = Integer.parseInt(request.getParameter("quantity"));
           int choice = Integer.parseInt(request.getParameter("choice"));
            
                       
            switch (choice){
            
             
                case 1 : ordertype = "ORDER BY p.description" ;
                break ; 
                
                case 2: ordertype = "ORDER BY p.name" ;
                break ;
                
                case 3 : ordertype = "ORDER BY p.price" ; 
                break ; 
                
                case 4 : ordertype = "ORDER BY p.stock ";
                break ; 
                
                
                
            }
            
            
          
                    
                  
                    List<ProductDTO> productNeedLoad = productService.findProductsUnderNewLimit(quantity, ordertype);
                    System.out.println("productNeedLoad");
                    System.out.println(productNeedLoad);
                    request.setAttribute("productNeedLoad", productNeedLoad);
                    request.getRequestDispatcher("admin.jsp").forward(request, response);

                    System.out.println("egyéb");

            }

        }

    

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
