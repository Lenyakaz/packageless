
package com.mycompany.packagelessshop.servlet;

import com.mycompany.packagelessshop.dto.LocationDTO;
import com.mycompany.packagelessshop.entity.LocationEntity;
import com.mycompany.packagelessshop.service.LocationService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Otto
 */
@WebServlet(name = "DriverServlet", urlPatterns = {"/DriverServlet"})
public class DriverServlet extends HttpServlet {

    @EJB
    private LocationService locationService ;
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DriverServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DriverServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        List<LocationDTO> locationList_wannaShop = locationService.findWannaShopIsBiggerThan(); //ez majd a LocationDTO-ból dolgozik, de most csak próba
        System.out.println("List_wannaShop-doGet");
        System.out.println(locationList_wannaShop);
        request.setAttribute("locationList_wannaShop", locationList_wannaShop);//ez tárolja a teljes plocation listát a jsp oldal számára
        request.getRequestDispatcher("driver.jsp").forward(request, response); //á
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        List<LocationDTO> locationListWannaShop = locationService.findWannaShopIsBiggerThan(); //ez majd a LocationDTO-ból dolgozik, de most csak próba
        System.out.println("WannaShop-doPost");
        System.out.println(locationListWannaShop);
        request.setAttribute("locationListWannaShop", locationListWannaShop);//ez tárolja a teljes plocation listát a jsp oldal számára
        request.getRequestDispatcher("driver.jsp").forward(request, response);
    }

    
     

}
