/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.servlet;

import com.mycompany.packagelessshop.dto.ProductDTO;
import com.mycompany.packagelessshop.filterbean.ListProductBean;
import com.mycompany.packagelessshop.service.ProductService;
import com.mycompany.packagelessshop.service.ShoppingService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ShoppingServlet", urlPatterns = {"/ShoppingServlet"})
public class ShoppingServlet extends HttpServlet {
    public static Long MAX_PRODUCT_ID=7L;
    
    @EJB 
    private ProductService productService;
    @EJB
    private ShoppingService shoppingService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        String getId = req.getParameter("product_id");
        if (getId == null || getId.isEmpty()) {
            //Első meghíváskor üres a product_id, ill. üres érték jelenik meg
            //Nem a legjobb megoldás itt zérózni, ezt meg kell oldani szebben
        } else { //ez az ág akkor működik, ha már kiválasztott egy terméket
            long longId = Long.parseLong(req.getParameter("product_id"));
            
            
            ProductDTO choosenProduct = productService.findById(longId);
            
            // ez kikeresi a product_id-hoz tartozó terméket és beleteszi a choosenProduct objektumba.
            
            req.getSession().setAttribute("choosenProduct", choosenProduct);
        }
        shoppingService.setWeights(req);
        if (req.getSession().getAttribute("numberOfProductInCart") == null) {
            req.getSession().setAttribute("numberOfProductInCart", 0);
            req.getSession().setAttribute("totalMoneyinCart", 0);
        }
        req.getRequestDispatcher("shopping.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        ListProductBean listProductBean = (ListProductBean) req.getSession().getAttribute("listProductBean");
        if (listProductBean == null) {
            listProductBean = new ListProductBean();
        }
        setupForJSP(req, listProductBean);
        
        req.getSession().setAttribute("listProductBean", listProductBean); //BH12Library módszer, de MŰKÖDIK!

        shoppingService.setWeights(req); //a lenti átírányítás előtt be kell állítani a kezdő súlyokat, különben elhasal
        req.getRequestDispatcher("shopping.jsp").forward(req, res);
    }

    public void setupForJSP(HttpServletRequest req, ListProductBean listProductBean) throws NumberFormatException {
        HttpSession session = req.getSession();
        long longId = Long.parseLong(req.getParameter("productId"));
        int numberOfProductInCart = (Integer) session.getAttribute("numberOfProductInCart");
        numberOfProductInCart++;
        session.setAttribute("numberOfProductInCart", numberOfProductInCart);
        double money = Double.parseDouble(req.getParameter("money"));
        int totalMoneyinCart = (Integer) session.getAttribute("totalMoneyinCart");
        totalMoneyinCart += money;
        session.setAttribute("totalMoneyinCart", totalMoneyinCart);

        listProductBean.setTotalMoneyinCart(totalMoneyinCart); //Összeg hozzáadás a bevásárló listához
        int weight = Integer.parseInt(req.getParameter("weight"));
        ProductDTO choosenProduct = (ProductDTO) session.getAttribute("choosenProduct");
        choosenProduct.setCalcWeight(weight);
        listProductBean.getProducts().add(choosenProduct); //Hozzáadjuk a kiválasztott terméket a bevásárló listához
        session.setAttribute("listProductBean", listProductBean); //lista átadás
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
