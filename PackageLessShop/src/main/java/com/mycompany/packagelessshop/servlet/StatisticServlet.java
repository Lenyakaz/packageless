package com.mycompany.packagelessshop.servlet;

import com.mycompany.packagelessshop.entity.ShoppingEntity_reserve;
import com.mycompany.packagelessshop.service.ShoppingService;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "StatisticServlet", urlPatterns = {"/StatisticServlet"})
public class StatisticServlet extends HttpServlet {

    @EJB
    private ShoppingService shoppingService;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StatisticServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StatisticServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /*
        List<ShoppingEntity_reserve> shoppingStat = shoppingService.findAll(); //még át kell DTO-zni rutinok megvannak
        request.setAttribute("shoppingStat", shoppingStat);
        request.getRequestDispatcher("statistic.jsp").forward(request, response);
         */
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println("megjöttem do-post");

        String button = request.getParameter("button");

        if ("button1".equals(button)) {

            List<ShoppingEntity_reserve> shoppingStat = shoppingService.findAll(); //még át kell DTO-zni rutinok megvannak
            request.setAttribute("shoppingStat", shoppingStat);
            request.getRequestDispatcher("statistic.jsp").forward(request, response);

        } else if ("button2".equals(button)) {
            System.out.println("5000 felett ");

            List<ShoppingEntity_reserve> shoppingStat = shoppingService.findSalesOverLimit(); //még át kell DTO-zni rutinok megvannak
            request.setAttribute("shoppingStat", shoppingStat);
            request.getRequestDispatcher("statistic.jsp").forward(request, response);

        } else {            // levalogatós limit alapján 

            String dateString = request.getParameter("date");
            Date date = null ; 
            
            try {
                System.out.println(dateString);
                date = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
                System.out.println(date);
            } catch (Exception e) {
                System.out.println("Hiba dátium parsolás közben!");
            }

            if (dateString == null || dateString.isEmpty()) {

                List<ShoppingEntity_reserve> shoppingStat = shoppingService.findAll();
                request.setAttribute("shoppingStat", shoppingStat);

            } else {

                List<ShoppingEntity_reserve> shoppingStat = shoppingService.findDateOfShopping(date);
                request.setAttribute("shoppingStat", shoppingStat);

            }

            request.getRequestDispatcher("statistic.jsp").forward(request, response);

        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
