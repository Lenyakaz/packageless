/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.filterbean;

import com.mycompany.packagelessshop.dto.ProductDTO;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author tbiro
 */
@Data
public class ListProductBean {

    List<ProductDTO> products = new ArrayList<>();
    int totalMoneyinCart;

    public void addToList(ProductDTO choosenProduct) {
        products.add(choosenProduct);
    }
}
