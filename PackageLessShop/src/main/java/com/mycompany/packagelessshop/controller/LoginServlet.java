
package com.mycompany.packagelessshop.controller;

import com.mycompany.packagelessshop.service.UserService;
import com.mycompany.packagelessshop.util.PassWordHashing;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

       
    @EJB
    UserService userservice;
    
     
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("welcome.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        PrintWriter pw = response.getWriter();

        PassWordHashing passwordhashing = new PassWordHashing () ; 
        boolean vmi = passwordhashing.logincheck(username,password);
        
        pw.println("sikeres login");
        pw.println(username);
        pw.println(password);
        
        
        
        if (vmi){
            
        
        request.getRequestDispatcher("/index.jsp").forward(request, response);
        
        }
        else {
            
            request.getRequestDispatcher("/invalidLogin.jsp").forward(request, response);
        }
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
