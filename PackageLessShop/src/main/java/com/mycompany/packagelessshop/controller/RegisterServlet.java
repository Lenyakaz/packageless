package com.mycompany.packagelessshop.controller;

import com.mycompany.packagelessshop.dao.UserDAOTest;
import com.mycompany.packagelessshop.util.PassWordHashing;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "RegisterServlet", urlPatterns = {"/RegisterServlet"})
public class RegisterServlet extends HttpServlet {


    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        //az input adatokat átadjuk lokális változónak
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        // HashedPassword implementation 
        
        PassWordHashing passwordhashing = new PassWordHashing () ; 
        // passwordhashing.signup(username,password);
        
        password = passwordhashing.signup(username,password);
        
        //String role = request.getParameter("role");
         
         UserDAOTest userdaotest = new UserDAOTest () ; 
         userdaotest.CreateUserEntity(username, email, password);

        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
