/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.service;

import com.mycompany.packagelessshop.dao.ProductDAO;
import com.mycompany.packagelessshop.dto.ProductDTO;
import com.mycompany.packagelessshop.entity.ProductEntity;
import com.mycompany.packagelessshop.mapper.ProductMapper;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

/**
 *
 * @author tbiro + ottika
 */
@Singleton //ha ezt bennehagyom, akkor más helyen nem használhatom fel :-) Miért jó nekem hogy Sinlgleton?
@TransactionAttribute //ez vajon mi a pöcs?
public class ProductService {

//    @EJB
//    private ProductDTO productDTO;
    
    @Inject
    private ProductDAO productDAO;

    public List<ProductDTO> listMapper(List<ProductEntity> productList) {
        ProductDTO productDTO = new ProductDTO(); //ez csak egy kamu beiktatott osztály, ha @EJB-t használok szétfossa magát
        productDTO.setProductDTOList(ProductMapper.INSTANCE.toDTOList(productList)); //meghívja a Mapstuctot
        return productDTO.getProductDTOList();
    }
    
    public List<ProductDTO> listMapperAnother (List<ProductEntity> productNeedLoad) {
        ProductDTO productDTO = new ProductDTO(); 
        productDTO.setProductDTOList(ProductMapper.INSTANCE.toDTOList(productNeedLoad)); //meghívja a Mapstuctot
        return productDTO.getProductDTOList();
    }
    

    public ProductDTO mapper(ProductEntity product) {
        ProductDTO productDTO = ProductMapper.INSTANCE.toDTO(product); //ez csak egy kamu beiktatott osztály, ha @EJB-t használok szétfossa magát
        return productDTO;
    }

    public List<ProductDTO> findAll() {
        return listMapper(productDAO.findAll()); //előtte: productDTO.getProductDTOList(); //az előbbi elkészült listát küldi tovább
    }
    
    public List<ProductDTO> findProductsUnderLimit(){
        
        return listMapperAnother(productDAO.findProductsUnderLimit()); 
    }
    
    public List<ProductDTO> findProductsUnderNewLimit(long quantity, String ordertype){
        
        return listMapperAnother(productDAO.findProductsUnderNewLimit(quantity,ordertype)); 
    }
    
    public List<ProductDTO> findByName(String name) {
        return listMapper(productDAO.getProductByNameWithTypedQuery(name));
    }
    
    public ProductDTO findById(long id) { //itt csak 1db lehet, felesleges a lista
        return mapper(productDAO.getProductByIdWithPlainQuery(id));
    }
    
    public boolean stockCheckAndMinusWeight(long id, int weightMinus) {
        ProductEntity productById = productDAO.getProductByIdWithPlainQuery(id); //itt nem használtam ProductDTO-t, mert nem a frontend felé megy az adat, hanem visszafelé
        int getStockById = productById.getStock();
        System.out.println("Eljutottam a készletellenőrzésig:  "+getStockById);
        if (getStockById > weightMinus) { //ha van elég, akkor itt csökkentjük a készletet
            productDAO.minusStock(productById, weightMinus);
            return true;
        } else {
            return false;
        }
    }

}
