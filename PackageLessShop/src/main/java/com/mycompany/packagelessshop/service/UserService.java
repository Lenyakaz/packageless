
package com.mycompany.packagelessshop.service;



import com.mycompany.packagelessshop.dao.UserDAO;
import com.mycompany.packagelessshop.entity.User;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Singleton
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class UserService {
    
    @Inject
    private UserDAO userDAO;
    
    public boolean checkUserCredentials(String username, String password) {
        
        //PassWordHashing passwordhashing = new PassWordHashing () ; 
        //passwordhashing.login(username, password) ; 
        
       
        
        
        User user = null;
        try {
            user = userDAO.getUser(username,password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return user != null;
        
    }
}
        
        
        
        
     //   metódus, ha az emailt adjuk meg mint beviteli adat és azt csekkoljuk
        
        /*            email.matches("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,}\\b");
        if (!email.contains("@")) {
            throw new RuntimeException("Hibás formátum: nincs @");
        }
        
        if (email.length() < 6) {
            throw new RuntimeException("Hibás formátum: nem elég hosszú");
        }

        System.out.println("userDao: " + userDAO);
        User user = null;
        try {
            user = userDAO.getUser(username,password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return user != null;
        
        
}

}
    
    */

