/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.service;

import com.mycompany.packagelessshop.dao.LocationDAO;
import com.mycompany.packagelessshop.dto.LocationDTO;
import com.mycompany.packagelessshop.entity.LocationEntity;
import com.mycompany.packagelessshop.mapper.LocationMapper;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

/**
 *
 * @author tbiro
 */
@Singleton
@TransactionAttribute
public class LocationService {
    
    @Inject
    private LocationDAO locationDAO;
    
    
    public List<LocationDTO> listMapper(List<LocationEntity> locationList) {
        LocationDTO locationDTO = new LocationDTO(); //ez csak egy kamu beiktatott osztály, ha @EJB-t használok szétfossa magát
        locationDTO.setLocationDTOList(LocationMapper.INSTANCE.toDTOList(locationList)); //meghívja a Mapstuctot
        return locationDTO.getLocationDTOList();
    }
    
    public List<LocationDTO> listMapperAnother (List<LocationEntity> productNeedLoad) {  // admin felület hivja meg az adminservleten keresztül
        LocationDTO productDTO = new LocationDTO(); 
        productDTO.setLocationDTOList(LocationMapper.INSTANCE.toDTOList(productNeedLoad)); //meghívja a Mapstuctot
        return productDTO.getLocationDTOList();
    }
    
    public List<LocationDTO> listMapperAnother_wannashop (List<LocationEntity> locationListWannaShop) { // driver servlet hivja meg a driver jsp oldalrol
        LocationDTO productDTO = new LocationDTO(); 
        productDTO.setLocationDTOList(LocationMapper.INSTANCE.toDTOList(locationListWannaShop)); //meghívja a Mapstuctot
        return productDTO.getLocationDTOList();
    }
    

    public LocationDTO mapper(LocationEntity location) {
        LocationDTO locationDTO = LocationMapper.INSTANCE.toDTO(location); //ez csak egy kamu beiktatott osztály, ha @EJB-t használok szétfossa magát
        return locationDTO;
    }
    
    public List<LocationEntity> findAll() {
        return locationDAO.findAll();
    }
    
    public List<LocationEntity> findAllEntity() {
        return locationDAO.findAllEntity();
    }
    
        
    public List<LocationDTO>findWannaShopIsBiggerThan(){
        return listMapperAnother_wannashop(locationDAO.findWannaShopIsBiggerThan());
    }
    
    
    
}
