/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.service;

import com.mycompany.packagelessshop.dto.ProductDTO;
import com.mycompany.packagelessshop.filterbean.ListProductBean;
import javax.ejb.Stateful;
import lombok.Data;

/**
 *
 * @author tbiro
 */
@Stateful
@Data
public class CartService {

    ListProductBean listProductBean = new ListProductBean();

}
