package com.mycompany.packagelessshop.service;

import com.mycompany.packagelessshop.dao.CartDAO;
import com.mycompany.packagelessshop.dao.ProductDAO;
import com.mycompany.packagelessshop.dao.ShoppingDAO;
import com.mycompany.packagelessshop.dto.ProductDTO;
import com.mycompany.packagelessshop.dto.ShoppingDTO;

import com.mycompany.packagelessshop.entity.LocationEntity;
import com.mycompany.packagelessshop.entity.ProductEntity;
import com.mycompany.packagelessshop.entity.ShoppingEntity;
import com.mycompany.packagelessshop.entity.ShoppingEntity_reserve;
import com.mycompany.packagelessshop.entity.User;
import com.mycompany.packagelessshop.entity.UserEntity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author tbiro + ottika
 */
@Singleton 
@TransactionAttribute
public class ShoppingService {

    private static final int MAX_EMPTY_BOX_WEIGHT_GRAMM = 300;
    private static final int MAX_LOADED_WEIGHT_GRAMM = 3000;
    private static ArrayList<ProductDTO> shoppingList; //ezt injektálni is lehetne?

    @Inject
    private ShoppingDAO shoppingDAO;
    @Inject
    private CartDAO cartDAO;
    @Inject
    private ProductDAO productDAO;
    @Inject
    private ProductService productService;
    
   

    public void setWeights(HttpServletRequest req) {
        int randomEmptyWeight = randomWeight(MAX_EMPTY_BOX_WEIGHT_GRAMM, 0);
        req.setAttribute("randomEmptyWeight", randomEmptyWeight); // súly paraméter átadás
        int randomLoadedWeight = randomWeight(MAX_LOADED_WEIGHT_GRAMM, MAX_EMPTY_BOX_WEIGHT_GRAMM);
        req.setAttribute("randomLoadedWeight", randomLoadedWeight); // súly paraméter átadás
        int weight = randomLoadedWeight - randomEmptyWeight;
        req.setAttribute("weight", weight);
    }

    public int randomWeight(int max, int min) {
        return (int) (Math.random() * (max - min) + min);
    }

    public static void setShoppingList(ProductDTO choosenProduct) { //ez még nem hívódik meg sehonnan...
        if (shoppingList.isEmpty() == true || shoppingList == null) {
            System.out.println("Üres a lista!");
            ArrayList<ProductDTO> shoppingList = new ArrayList<>();
        }
        shoppingList.add(choosenProduct);
        System.out.println(choosenProduct);
    }

    public ShoppingEntity setShopping(int moneySpent, LocationEntity location, UserEntity user) {
        return shoppingDAO.newShopping(moneySpent, location, user);
    }

    public void setCart(List<ProductDTO> shoppingCart, ShoppingEntity whichShopping) {
        for (ProductDTO i : shoppingCart) {
            ProductEntity whichProduct = productDAO.getProductByIdWithPlainQuery(i.getId()); //lehet ez nem olyan elegáns, hogy adatbázislekérdezésből hozzuk elő
            cartDAO.newCart(i, whichShopping, whichProduct);

            //A készlet ellenőrzés elég nevetséges, ha a valóságban már kimérte a készletről fizikailag a terméket a dobozba
            //és akkor szólnánk, hogy nincs annyi készleten :-)
            //Ettől függetlenül kell ellenőrízni és az admin felületen jelezni, hogy minusba megy a készlet.
            boolean isStockEnough = productService.stockCheckAndMinusWeight(i.getId(), i.getCalcWeight());
        }
    }
    //--------------------------------Otto cuccai -----------------
    public List<ShoppingEntity_reserve> findAll() {
        System.out.println("sopping service findAll");
        return shoppingDAO.findAll(); // elkészült listát küldi tovább
    }
    
    public List<ShoppingEntity_reserve> findDateOfShopping(Date date) {
        System.out.println("dátumoslekerősben vagyok - soppingservice");
        return shoppingDAO.findDateOfShopping(date); // elkészült listát küldi tovább
    }
    
    public List<ShoppingEntity_reserve> findSalesOverLimit(){
        return shoppingDAO.findSalesOverLimit();
    }
    
    public List<ShoppingEntity_reserve> ShoppingEntityByName(String string){
        return shoppingDAO.ShoppingEntityByName(string);
    }
    
    
 
    
    /*
    public List<ShoppingDTO> listMapper(List<ShoppingEntity> shoppingStat) {
    ShoppingDTO shoppingDTO = new ShoppingDTO(); 
    shoppingDTO.setShoppingDTOList(ShoppingMapper.INSTANCE.toDTOList(shoppingStat));
    return shoppingDTO.getShoppingDTOList();
    
    }*///  még fog kelleni ha átdaozzuk ezt is 
   
}
