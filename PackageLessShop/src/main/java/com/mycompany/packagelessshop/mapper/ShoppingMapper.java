
package com.mycompany.packagelessshop.mapper;

import com.mycompany.packagelessshop.dto.ShoppingDTO;
import com.mycompany.packagelessshop.entity.ShoppingEntity;
import com.mycompany.packagelessshop.entity.ShoppingEntity_reserve;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author Otto
 */
@Mapper
public interface ShoppingMapper {
   
    ShoppingMapper INSTANCE = Mappers.getMapper(ShoppingMapper.class); // Interface típusú objektum
    /*
    public ShoppingDTO toDTO(ShoppingEntity shoppingEntity); 
    public List<ShoppingDTO> toDTOList(List<ShoppingEntity> shoppingEntities); //DTO-vá alakítjuk az Enity-ket
     */  // még kell !!!!
    
    
    public ShoppingDTO toDTO(ShoppingEntity_reserve shoppingEntity_reserve); 
    public List<ShoppingDTO> toDTOList(List<ShoppingEntity_reserve> shoppingEntities);

    

        
}
