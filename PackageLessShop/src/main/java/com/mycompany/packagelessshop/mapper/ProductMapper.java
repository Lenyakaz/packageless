/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.mapper;

import com.mycompany.packagelessshop.dto.ProductDTO;
import com.mycompany.packagelessshop.entity.ProductEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class); // Interface típusú objektum, de mit csinál?

    public ProductDTO toDTO(ProductEntity productEntity); //hol fejtjük ki ezt?

    public List<ProductDTO> toDTOList(List<ProductEntity> productEntities); //DTO-vá alakítjuk az Enity-ket
    //ez a ProductDTO listát adja vissza,
    //ebben lesznek benne a találatok?
                              //vár egy Entity listát
}