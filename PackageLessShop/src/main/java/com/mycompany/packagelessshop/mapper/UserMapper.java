/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.mapper;

import com.mycompany.packagelessshop.dto.UserDTO;
import com.mycompany.packagelessshop.entity.UserEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author Otto
 */
@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class); 

    public UserDTO toDTO(UserEntity usertEntity); 

    public List<UserDTO> toDTOList(List<UserEntity> userEntities); //DTO-vá alakítjuk az Enity-ket
}