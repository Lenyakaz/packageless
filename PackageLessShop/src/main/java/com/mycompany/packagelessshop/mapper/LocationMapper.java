/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.mapper;

import com.mycompany.packagelessshop.dto.LocationDTO;
import com.mycompany.packagelessshop.entity.LocationEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author tbiro
 */
@Mapper
public interface LocationMapper {
    
    LocationMapper INSTANCE = Mappers.getMapper(LocationMapper.class); 

    public LocationDTO toDTO(LocationEntity locationEntity); 

    public List<LocationDTO> toDTOList(List<LocationEntity> locationEntities); 
}
