
package com.mycompany.packagelessshop.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;



public class PassWordHashing {

    
    public static HashMap<String, String> DB = new HashMap<String, String>();
    public static final String SALT = "my-salt-text";

    
    	public boolean logincheck(String username,String password) {
        
        boolean logincheck = false ; 
        
        PassWordHashing test = new PassWordHashing () ;
        //test.signup(username,password);

		// login should succeed.
		if (test.login(username,password)){
			System.out.println("user login successfull.");
                        logincheck = true ; 
                        
                }
                else{
			System.out.println("user login failed de miért is ?");
                        logincheck = false ; 
                }
                return logincheck ;
	}

	public String signup(String username, String password) {
		String saltedPassword = SALT + password;
		String hashedPassword = generateHash(saltedPassword);
		DB.put(username, hashedPassword);
                System.out.println(username +" "+ password );
                System.out.println( "regisztrációkor generált " + hashedPassword);
                System.out.println("használt salt " + SALT);
                return hashedPassword; 
	}

	public Boolean login(String username, String password) {
		Boolean isAuthenticated = false;

		// remember to use the same SALT value use used while storing password
		// for the first time.
		String saltedPassword = SALT+password;
		String hashedPassword = generateHash(saltedPassword);

		String storedPasswordHash = (String) DB.get(username);
                System.out.println("loginkor salt"+SALT);
                System.out.println(username +"-"+ password );
                System.out.println("login hashedpassword amit frissen general " + hashedPassword);
                System.out.println("login elraktározott hashed"+storedPasswordHash);
                
		if(hashedPassword.equals(storedPasswordHash)){
			isAuthenticated = true;
		}else{
			isAuthenticated = false;
		}
		return isAuthenticated;
	}

	public static String generateHash(String input) {
		StringBuilder hash = new StringBuilder();

		try {
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			byte[] hashedBytes = sha.digest(input.getBytes());
			char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'a', 'b', 'c', 'd', 'e', 'f' };
			for (int idx = 0; idx < hashedBytes.length;   idx++) {
				byte b = hashedBytes[idx];
				hash.append(digits[(b & 0xf0) >> 4]);
				hash.append(digits[b & 0x0f]);
			}
		} catch (NoSuchAlgorithmException e) {
			// handle error here.
		}

		return hash.toString();
	}

}
    
    

