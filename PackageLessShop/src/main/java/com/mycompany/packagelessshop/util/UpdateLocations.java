
package com.mycompany.packagelessshop.util;

import com.mycompany.packagelessshop.entity.LocationEntity;
import com.mycompany.packagelessshop.service.LocationService;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

@Singleton
@TransactionAttribute
public class UpdateLocations {
    
  
@EJB
LocationService locationService;
    
    @PersistenceContext
    private EntityManager em;
    
    public boolean LocationUpdate (long district, long quantity) {
    
    EntityManagerFactory factory = Persistence.createEntityManagerFactory("packagelessshop");
    EntityManager entityManager = factory.createEntityManager();
    entityManager.getTransaction().begin() ; 
      
      
      
      LocationEntity locationentity = entityManager.find( LocationEntity.class, district );
      
        
      //before update
      
      locationentity.setWannaShop (locationentity.getWannaShop()+quantity);
      entityManager.getTransaction( ).commit( );
      
      
      entityManager.close();
      factory.close();
      return true; 
      
      
   }
}
    
    
  
    

