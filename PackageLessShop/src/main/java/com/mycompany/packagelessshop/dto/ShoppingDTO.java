
package com.mycompany.packagelessshop.dto;

import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author Otto
 */



public class ShoppingDTO {
    
    private List<ShoppingDTO> shoppingDTOList; //ebbe gyűjtjük a frontend felé-felől a vásárlásokat
    private Long id;
    private LocalDateTime dateAndTime;
    private double moneySpent;
    private Long location_id ; 
    private Long Uid ; 
    
    
    public void addToShoppingDTOList (ShoppingDTO choosenProduct){
        shoppingDTOList.add(choosenProduct);
    }   

    public List<ShoppingDTO> getShoppingDTOList() {
        System.out.println("már a dto is megvan megyek visszafelé");
        return shoppingDTOList;
    }

    public void setShoppingDTOList(List<ShoppingDTO> shoppingDTOList) {
        this.shoppingDTOList = shoppingDTOList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(LocalDateTime dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public double getMoneySpent() {
        return moneySpent;
    }

    public void setMoneySpent(double moneySpent) {
        this.moneySpent = moneySpent;
    }

    public Long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Long location_id) {
        this.location_id = location_id;
    }

    public Long getUid() {
        return Uid;
    }

    public void setUid(Long Uid) {
        this.Uid = Uid;
    }
    
    
    
}
    
    

