/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.dto;

import java.util.List;
import lombok.Data;

/**
 *
 * @author tbiro
 */
@Data
public class ProductDTO {
    
    private List<ProductDTO> productDTOList; //ebbe gyűjtjük a frontend felé-felől a termékeket
    private Long id;
    private String name;
    private String description;
    private double price;
    private int stock;  

    private int calcWeight; //kell a vásárolt mennyiség rögzítéséhez
    
    public void addToProductDTOList (ProductDTO chossenProduct){
        productDTOList.add(chossenProduct);
    }
}
