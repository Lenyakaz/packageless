/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packagelessshop.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import lombok.Data;

/**
 *
 * @author tbiro
 */
@Data
public class LocationDTO {
    
    private Long id;
    private List<LocationDTO> locationDTOList;
    
    private String address;
    private int district;
    private LocalDate shoppingDate;
    private LocalTime shoppingTime;
    private int wannaShop ; 
    
}
