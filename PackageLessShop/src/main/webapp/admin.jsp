<%-- 
    Document   : admin
    Created on : 2020.08.10., 16:07:35
    Author     : tbiro & ottika
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Kifogyó Termékek</title>
        <style>

            #locations {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;

            }

            #locations td, #locations th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #locations tr:nth-child(even){background-color: #bfbfbf;}

            #locations tr:hover {background-color: #ddd;}

            #locations th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color:#a04646;
                color: white;
            }

            h1 {
                font-size:45px;
                color: #fff;
                text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.5);
            }


            h2 {
                font-size:75px;
                color: #f08d3c;
                text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.7);
            }

            p {
                font-size:20px;
                margin-top:0;
                margin-bottom:0;
                color: #fff;
                text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.1);
            }


            bg { /* The image used */
                /* background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQmBzZMZtm0TGbEKnQwOoVPANmlme9LhZTT-amIUDvMTwGQUIoP"); */
                background-image: url("Images/FaceDown_wallpaper_background_ID_77701450778.jpg");
                /* Full height */
                height: 100%; 

                /* Center and scale the image nicely */
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover; }
            body {
                background-image: url("Images/FaceDown_wallpaper_background_ID_77701450778.jpg");
            }



        </style>
        <%@include file="WEB-INF/head.jsp" %>

    <body>

        <%@include file="WEB-INF/menu.jsp" %>

        <h1>Admin felület lekérdezések,statisztikák</h1>   
        <h2> Beállítások* </h2>

        <form action="${pageContext.request.contextPath}/AdminServlet" method="post">
            <button type="submit" name="button" value="button1">Termékek full készletlistája</button>



            <label for="quantity" >Minimum limit (gr):</label>
            <input type="number" id="quantity" name="quantity" value=300000 min="0" max="500000" step="500">
            <input type="number" id="choice" name="choice" value=1 min="1" max="4" step="1">
            <input type="submit" value="Véglegesítem">


            <button type="submit" name="button" value="button2">Beállítások*</button>
            <button type="submit" name="button" value="button3">Statisztikák lekérdezések</button>




        </form>


        <table id="locations">
            <tr>
                <th> Leírás </th>
                <th> Termék neve </th>
                <th> Termék ára HUF/1000gr </th>
                <th> Raktárkészlet gr. </th>



            </tr>

            <c:forEach items="${productNeedLoad}" var="p">

                <tr>

                    <td><c:out value="${p.description}"></c:out></td>
                    <td><c:out value="${p.name}"></c:out></td>
                    <td><c:out value="${p.price}"></c:out></td>
                    <td><c:out value="${p.stock}"></c:out></td>


                </c:forEach>



            </tr> 

        </table>


        <br>
    </body>
</html>

