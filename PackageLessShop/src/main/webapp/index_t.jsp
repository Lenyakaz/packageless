<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Csomagolásmentes Bolt</title>
        <link rel="StyleSheet" type="text/css" href="style.css">
        <%@include file="WEB-INF/head.jsp" %>
        <style>
        body {background-color: #9494b8;
         background-image: url("Images/close-up-photo-of-berries-1379636 (1).jpg");
		  /* Full height */
  height: 100%; 

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;}
    </style>
    </head>
    <body>
    
        
        <div class="container">
            <%@include file="WEB-INF/menu.jsp" %>
            <div class="row">
                <div class="col-4">
                    
                    <p>
                        Így néz ki a sok jó bio áru! <br />
                        Csak rád vár!  
                    </p>
                </div>
                <div class="col-8">
                    
                    <h1>Csomagolásmentes Bolt</h1>
                    <br>
                    <br>
                    <br>
                    <h2>Használj újrahasznosított dobozokat! Hozd el hozzánk, töltsd meg! Vásárolj egézséges dolgokat!</h2>
                    <br>
                    <br>
                    </br>
                    <h2>Greta Thunberg ajánlásával</h2>

                    <br>
                    <br>
                    </br>
                    
                </div>
            </div>
        </div>
    </body>
</html>
