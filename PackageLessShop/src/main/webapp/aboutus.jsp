<%-- 
    Document   : driver
    Created on : 2020.08.10., 16:18:01
    Author     : tbiro és ottika
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Kifogyó Termékek</title>
        <style>

            #locations {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
                color: white;
            }

            #locations td, #locations th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #locations tr:nth-child(even){background-color: #bfbfbf;}

            #locations tr:hover {background-color: #ddd;}

            #locations th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color:#a04646;
                color: white;
            }



             h1 {
            font-size:45px;
            color: #fff;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.5);
        }

        h2 {
            font-size:75px;
            color: #f08d3c;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.7);
        }

        p {
            font-size:20px;
            margin-top:0;
            margin-bottom:0;
            color: #fff;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.1);
        }

           
         
             h3 {
                font-size:38px;
                color: #fff;
                text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.5);
            }



            bg { /* The image used */
                /* background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQmBzZMZtm0TGbEKnQwOoVPANmlme9LhZTT-amIUDvMTwGQUIoP"); */
                background-image: url("Images/wallpaper2you_109415.jpg");
                /* Full height */
                height: 100%; 

                /* Center and scale the image nicely */
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover; }
            body {
                background-image: url("Images/wallpaper-mania.com_High_resolution_wallpaper_background_ID_77701450778.jpg");
            }

        </style>
        <%@include file="WEB-INF/head.jsp" %>

    <body>

        <%@include file="WEB-INF/menu.jsp" %>

        <h1>A packageless csoport tagjai :</h1> 
        
        <h2>Bíró Tamás a "főnök" - ötletgazda és projektfelelős</h2>  
        <p>Privát életben hajcsár és börtönőr aki napszaktól függetlenül képes bármikor 
            egy mattermost-os körlevelet kibocsátani magából amitől azonnal lelkiismeretfurdalása lesz az embernek , 
            hogy nem is dolgozik. Egy vadnyugati pisztolyhős sebességét lepipálva irja át a komplett kódot akár az éj leple alatt
            ,bárkijét - hogy mire befejezem a for ciklus begépelését már többé nem fut le, mert időközben sokkal 
            szebb nevet adott az összes entitásnak mezőnek és classnak .....</p>
    
        <h2>Mikó Alex a sztoikus nyugalom Git manager és entitáskészítő</h2>
        <p>Sajnos Alex mostanában betegeskedett, így leginkább on-line tudtunk vele dolgozni - de örök nyugalma optimizmusa a legnehezebb 
           pillanatokban is segített. Legendás tisztánlátásával és lényegretörésével oldotta meg az elkadásokat amikor már feladtam 
           volna . Ilyenkor a legjobb tanácsokat adta, hogy " ...kurd bele egy entitásba és kész " vagy nyomj neki egy Stash-t és szarjál rá, jó lesz!" 
           örökérvényű tanácsai átsegitettek a legnehezebb periódusokon is</p>
           
        <h2>Lévai Ottó , a délutános kódeltakarító, vizhordó és auto-pilot kimutatáskészítő</h2>
        <p>Aki hosszú napokat éjszakákat nem kimélve próbálta feltartani a Git kiméletlen pusztítását  
           miközben többször sodródott az epilepszis rohamig amikor egy vidám mergelés után annyit irt ki 
           a 40.000 soros program 35 perces deployálás után a 10 oldalas rendezett táblázat helyett ,hogy "Hello World" ... 
           Akit mindez nem tört meg és kidolgozta a humor nélkül képtelenség programozni mert meghülyülsz interface-t</p>
        
       
         <h1>Kössz srácok ez emlékezetes meló volt !</h1>
         <p> 2020 szeptember .... </p>
       
    </body>   
</html>