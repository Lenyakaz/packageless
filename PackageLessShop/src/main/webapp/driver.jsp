<%-- 
    Document   : driver
    Created on : 2020.08.10., 16:18:01
    Author     : tbiro és ottika
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Kifogyó Termékek</title>
        <style>

            #locations {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
                color: white;
            }

            #locations td, #locations th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #locations tr:nth-child(even){background-color: #bfbfbf;}

            #locations tr:hover {background-color: #ddd;}

            #locations th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color:#a04646;
                color: white;
            }



            h1 {
                font-size:45px;
                color: #fff;
                text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.5);
            }

            h2 {
                font-size:75px;
                color: #f08d3c;
                text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.7);
            }

            p {
                font-size:20px;
                margin-top:0;
                margin-bottom:0;
                color: #fff;
                text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.1);
            }


            bg { /* The image used */
                /* background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQmBzZMZtm0TGbEKnQwOoVPANmlme9LhZTT-amIUDvMTwGQUIoP"); */
                background-image: url("Images/wallpaper2you_109415.jpg");
                /* Full height */
                height: 100%; 

                /* Center and scale the image nicely */
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover; }
            body {
                background-image: url("Images/wallpaper-mania.com_High_resolution_wallpaper_background_ID_77701450778.jpg");
            }

        </style>
        <%@include file="WEB-INF/head.jsp" %>





    <body>

        <%@include file="WEB-INF/menu.jsp" %>

        <h1>Mobil-shop megállói az időpontok és a regisztrációk függvényében !</h1>
         <h2>Dátum majd időrendi leválogatással !</h2>
         
         
        <table id="locations">
            <tr>
                <th> Cím </th>
                <th> Kerület </th>
                <th> Dátum </th>
                <th> Időpont </th>
                <th> Az adott napra regisztrált vásárlók száma</th>


            </tr>

            <c:forEach items="${locationList_wannaShop}" var="l">

                <tr>

                    <td><c:out value="${l.address}"></c:out></td>
                    <td><c:out value="${l.district}"></c:out></td>
                    <td><c:out value="${l.shoppingDate}"></c:out></td>
                    <td><c:out value="${l.shoppingTime}"></c:out></td>
                    <td><c:out value="${l.wannaShop}"></c:out></td>

                </c:forEach>



            </tr> 





            <!--            <form method="get" action="chooseLocation">
            <th colspan="1">
                <input type="submit" value="Jelentkezem erre a címre!" ${sessionScope.dept eq 'user.isAbleToChooseLocation' ? 'false' : 'disabled' }/>
            </th>  
            <th>
            ${sessionScope.dept}
        </th>
    </form>-->
        </table>


        <br>
    </body>
</html>
