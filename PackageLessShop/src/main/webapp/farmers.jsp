<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link href='https://fonts.googleapis.com/css?family=Averia Gruesa Libre' rel='stylesheet'>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}
body, html {
font-family: 'Averia Gruesa Libre';
	height: 100%;
}


input{
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}
.bg {
  /* The image used */
  /* background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQmBzZMZtm0TGbEKnQwOoVPANmlme9LhZTT-amIUDvMTwGQUIoP"); */
	background-image: url("Images/close-up-photo-of-berries-1379636 (1).jpg");
		  /* Full height */

height:100%;
  background-position: center;
  background-repeat:repeat;
  background-size:cover;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  width:50%;
  left:28%;
  position:relative;
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
  margin-bottom: 0;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-70 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-70, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
</head>
<body class="bg">
<div class="container">
<h1>${ err }</h1>
  <form action="registrationFarmer.rsvp" method="post" enctype="multipart/form-data" onsubmit="return validatePassword()">
  <label>Personal Details</label>
    <div class="row">
      <div class="">
        <label for="name">Name</label>
      </div>
      <div class="">
        <input type="text" id="name" name="farmerFullName" required placeholder="Your name..">
      </div>
    </div>
    
    <div class="">
      <div class="">
        <label for="email">email</label>
      </div>
      <div class="">
        <input type="email" id="email" name="email" required placeholder="eg. abc@xyz.com">
      </div>
    </div>
    
      <div class="">
        <label for="pass">password</label>
      </div>
      <div class="">
       <input type="password" required id="pass" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" name="password" placeholder="ex.Absc@12SX"> 
      </div>
    </div>
    <div class="">
      <div class="">
        <label for="passw">confirm password</label>
      </div>
      <div class="">
       <input type="password" required id="passw" name="confirmpassword" placeholder="ex.Absc@12SX"> 
      </div>
    </div>
    <div class="">
      <input type="submit" value="Submit">
    </div>

</body>


<script>
function validatePassword(){
	var password= document.getElementById("pass").value;
	var confirmPassword = document.getElementById("passw").value;

	if(password == confirmPassword)
	return true;
	else{
	alert("Password didn't match");
	return false;
	}

	}</script>


</html>

