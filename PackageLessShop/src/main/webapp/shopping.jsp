<%-- 
    Document   : shopping
    Created on : 2020.08.10., 15:46:17
    Author     : tbiro
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <% int randomEmptyWeight = (Integer) request.getAttribute("randomEmptyWeight"); %>
        <% int randomLoadedWeight = (Integer) request.getAttribute("randomLoadedWeight");%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Helyszíni vásárlás</title>
        <%@include file="WEB-INF/head.jsp" %>

    </head>

    <body>
        <%@include file="WEB-INF/menu.jsp" %>
        <h2>Ezen az oldalon tudsz terméket választani és lemérni a súlyát</h2>
        <table id="shopping_order" class="table table-borderless">
            <tr id="headlines" style="table-row">
                <th>Sorrend:</th>
                <th>Kijelző</th>
                <th>Üzenet</th>
            </tr>
            <tr id="idIn" style="table-row">
                <td>
                    <p> 1, Írd be a vásárolni kívánt áru kódját:</p>
                </td>
                <td>
                    <form class="form-inline" method="Get" onsubmit="unHide2()">
                        <!--                        Az a gond a method="get"-tel, hogy a bevitel után visszatér automatikusan a hívó servletre, emiatt újrakezdi a képernyőt!-->
                        <label for="product_id">Áru kódja:</label>
                        <input type="number" class="form-control" id="product_id" name="product_id">  
                        <input type="submit" value="Nyomj entert!">
                    </form>
<!--                <script>
                        function unHide2() {
                            alert(doNotHide2.value);
                            if (doNotHide2.value==1){
                                document.getElementById("putEmptyBox").style.display = 'table-row';
                            }
                        }
                </script>-->
                </td>
                <td>
                    <p>Választott termék: <span style="font-size: 30px"><c:out value="${choosenProduct.name}"></c:out></span></p>
                </td>
            </tr>
            <tr id="putEmptyBox" style="table-row">
                <td>
                    <p> 2, Tedd a mérlegre az üres dobozodat:</p>
                </td>
                <td>
                    <p> Üres súly:<span class="border border-primary"> <span class="code"> 000</span>              
                            gramm</span></p>
                </td>
                <td>
                    <button type="button" class="btn btn-primary" onclick="measureEmptyWeight()">TOVÁBB</button>
                    <script>
                        function measureEmptyWeight() {
                            var randomEmptyWeight =<%= randomEmptyWeight%>;
                            $({countNum: $('.code').html()}).animate({countNum: randomEmptyWeight}, {
                                duration: 1000,
                                easing: 'linear',
                                step: function () {
                                    $('.code').html(Math.floor(this.countNum));
                                },
                                complete: function () {
                                    $('.code').html(this.countNum);
                                    //alert('finished');
                                }
                            });
                            document.getElementById("3.weight").style.display = 'table-row';
                        }
                    </script>
            </tr>            

            <tr id="3.weight" style="display:none">
                <td>
                    <p> 3, Töltsd meg. Tedd a mérlegre :</p>
                </td>
                <td>
                    <p> Súly:<span class="border border-primary"> <span class="mode"> 000</span>              
                            gramm</span></p>
                </td>
            <script>
                function measureLoadedWeight() {
                    var randomLoadedWeight =<%= randomLoadedWeight%>;
                    $({countNum: $('.mode').html()}).animate({countNum: randomLoadedWeight}, {
                        duration: 1000,
                        easing: 'linear',
                        step: function () {
                            $('.mode').html(Math.floor(this.countNum));
                        },
                        complete: function () {
                            $('.mode').html(this.countNum);
                            //alert('finished');
                        }
                    });
                    document.getElementById("4.listProduct").style.display = 'table-row';
                }
            </script>
            <td>
                <button type="button" class="btn btn-primary" onclick="measureLoadedWeight()">TOVÁBB</button>
            </td>
        </tr>
        <tr id="4.listProduct" style="display:none">>
            <td>
                <p> 4, <span class="border border-primary">
                        <c:out value="${choosenProduct.id}"></c:out>
                    </span>.kód <span class="border border-primary"> <c:out value="${choosenProduct.name}"></c:out>         
                    </span>: <span class="border border-primary">  <c:out value="${weight/1000}"></c:out> kg x   
                        <c:out value="${choosenProduct.price}"></c:out> Ft/kg </span> = :</p>
                </td>
                <td>
                    <p> Ár: <span class="border border-primary"><c:out value="${weight/1000*choosenProduct.price}"></c:out>
                        Ft</span></p>
            </td>
            <td>
<!--                Ez küldi át a szükséges adatokat a kosárhoz-->
                <form action="ShoppingServlet" method="post"> 
                <button type="submit" class="btn btn-success">KOSÁRBA</button>
                <input type="hidden" id="productId" name="productId" value="${choosenProduct.id}">
                <input type="hidden" id="money" name="money" value="${weight/1000*choosenProduct.price}">
                <input type="hidden" id="weight" name="weight" value="${weight}">

<!--                az órai RemoveMessage-ben így ment át linkkel:
                <a class="button" href="removeMessage?messageId=${message.id}">removeMessage</a>-->
                </form>
            </td>
        </tr>
        <tr id="5.summarise" style="table-row">
            <td>
                <h4>A kosár eddigi tartalma:</h4>
            </td>
            <td>
                <h4><span class="border border-primary"> <c:out value="${numberOfProductInCart}"></c:out>
                    </span> fajta tétel <span class="border border-primary"> 
                    <c:out value="${totalMoneyinCart}"></c:out> </span> Ft összegben</h4>
            </td>
            <td>
                <form action="CustomerServlet" method="get"> 
                <button type="submit" class="btn btn-warning">VÁSÁRLÁS BEFEJEZÉSE, FIZETÉS</button>
                <!--<input type="hidden" id="listProductBean" name="listProductBean" value="${listProductBean}">-->
<!--                A vásárlás befelyezős servletnek még nem ad át semmit...folyamatban
                <input type="hidden" id="productId" name="productId" value="${choosenProduct.id}">
                <input type="hidden" id="money" name="money" value="${weight/1000*choosenProduct.price}">
                <input type="hidden" id="weight" name="weight" value="${weight}">-->
                </form>
            </td>
        </tr>
    </table>
    <img class="fixed-bottom" src="https://www.penztargepforras.hu/toner/feltolt/termek/kep/n_226750_1.jpg" 
         style="width:500px;">
</body>
</html>
