<%-- 
    Document   : login
    Created on : 2020.08.10., 16:12:21
    Author     : tbiro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <%@include file="WEB-INF/head.jsp" %>
    </head>
    <body>
        <%@include file="WEB-INF/menu.jsp" %>
        <h1>Itt tudsz bejelentkezni</h1>
        <form>
            <form action="login" method="post">
            <input type="text" name="username">
            <input type="password" name="password">
             <input type="submit" value="login">
             <span class="error">${error}</span>
            </form>
            
            <br>
            <p>Új felhasználó? Klikk ide a regisztrációért!</p>
            <button type="button">Regisztráció</button> 
     
    </body>
</html>
