<%-- 
    Document   : registerpage_
    Created on : 2020.08.30., 19:48:03
    Author     : Otto
--%>

<!DOCTYPE html>
<html>
    <style>
        body {font-family: Arial, Helvetica, sans-serif;}
        * {box-sizing: border-box;}

        /* Full-width input fields */
        input[type=text], input[type=password] {
            width: 100%;
            padding: 15px;
            margin: 5px 0 22px 0;
            display: inline-block;
            border: none;
            background: #f1f1f1;
        }

        /* Add a background color when the inputs get focus */
        input[type=text]:focus, input[type=password]:focus {
            background-color: #ddd;
            outline: none;
        }

        /* Set a style for all buttons */
        button {
            background-color: #ac3939;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
            opacity: 0.9;
        }

        button:hover {
            opacity:1;
        }

        /* Extra styles for the cancel button */
        .cancelbtn {
            padding: 14px 20px;
            background-color: #4d88ff;
        }

        /* Float cancel and signup buttons and add an equal width */
        .cancelbtn, .signupbtn {
            float: left;
            width: 50%;
        }

        /* Add padding to container elements */
        .container {
            padding: 16px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: #ffcc99;
            padding-top: 50px;
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #bfbfbf;
            margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
            border: 1px solid #888;
            width: 80%; /* Could be more or less, depending on screen size */
        }

        /* Style the horizontal ruler */
        hr {
            border: 1px solid #f1f1f1;
            margin-bottom: 25px;
        }

        /* The Close Button (x) */
        .close {
            position: absolute;
            right: 35px;
            top: 15px;
            font-size: 40px;
            font-weight: bold;
            color: #f1f1f1;
        }

        .close:hover,
        .close:focus {
            color: #f44336;
            cursor: pointer;
        }

        /* Clear floats */
        .clearfix::after {
            content: "";
            clear: both;
            display: table;
        }

        h1 {
            font-size:45px;
            color: #fff;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.5);
        }

        h2 {
            font-size:75px;
            color: #f08d3c;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.7);
        }

        p {
            font-size:20px;
            margin-top:0;
            margin-bottom:0;
            color: #fff;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.1);
        }


        /* Change styles for cancel button and signup button on extra small screens */
        @media screen and (max-width: 300px) {
            .cancelbtn, .signupbtn {
                width: 100%;
            }
        }

        body {
            background-image: url("Images/wallpaper-mania.com_High_resolution_wallpaper_background_ID_77701450778.jpg");
        } 


    </style>
    <body>


        <h2>Packageless Register Form</h2>

        <button onclick="document.getElementById('id01').style.display = 'block'" style="width:auto;">Sign Up</button>





        <div id="id01" class="modal">




            <span onclick="document.getElementById('id01').style.display = 'none'" class="close" title="Close Modal">&times;</span>
            <form class="modal-content" action="RegisterServlet" method="get" >
                <div class="container">


                    <h1>Registration</h1>

                    <p>Date : <span id="datetime"></span></p>

                    <script>
                        var dt = new Date();
                        document.getElementById("datetime").innerHTML = dt.toLocaleDateString();
                    </script>




                    <p>Please fill in this form to create your account.</p>
                    <hr>
                    <form>
                        <label for="username"><b>Username</b></label>
                        <input type="text" placeholder="Enter Username" name="username" required>

                        <label for="email"><b>Email</b></label>
                        <input type="text" placeholder="abc@xyz.com" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required >

                        <label for="password"><b>Password</b></label>
                        <input type="password" placeholder="password here" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" 
                               title="Must contain at least one  number and one uppercase and lowercase letter, and at least 6 or more characters" required>

                        <label for="password-repeat"><b>Repeat Password</b></label>
                        <input type="password" placeholder="repeat your password" name="password-repeat" required>


                        <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

                        <div class="clearfix">
                            <button type="button" onclick="document.getElementById('id01').style.display = 'none'" class="cancelbtn">Cancel</button>
                            <button type="submit" class="signupbtn">Sign Up</button>
                        </div>
                </div>
            </form>
        </div>

        <script>
            // Get the modal
            var modal = document.getElementById('id01');

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        </script>

    </body>
</html>
