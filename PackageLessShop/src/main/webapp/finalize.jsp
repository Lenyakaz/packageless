

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Finalize</title>
        <style>
            #products {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            #products td, #products th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #products tr:nth-child(even){background-color: #f2f2f2;}

            #products tr:hover {background-color: #ddd;}

            #products th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #4CAF50;
                color: white;
            }
        </style>
        <%@include file="WEB-INF/head.jsp" %>
    </head>

    <body>
        <%@include file="WEB-INF/menu.jsp" %>
        <h1>A bevásárlólistád tartalma:</h1>
        <table id="products">
            <tr>
                <th>Termék azonosító</th>
                <th>Név</th>
                <th>Leírás</th>
                <th>Egységár(Ft/kg)</th>
                <th>Vásárolt mennység (gramm)</th>
                <th>Részösszeg (Ft)</th>
            </tr>
            <c:forEach items="${listProductBean.products}" var="p"> 
                <tr>
                    <td><c:out value="${p.id}"></c:out></td>
                    <td><c:out value="${p.name}"></c:out></td>
                    <td><c:out value="${p.description}"></c:out></td>
                    <td><c:out value="${p.price}"></c:out></td>
                    <td><c:out value="${p.calcWeight}"></c:out></td>
                    <td><c:out value="${p.calcWeight/1000*p.price}"></c:out></td>
                    </tr>  
            </c:forEach>
        </table>
        <h1>Összesen:<c:out value="${listProductBean.totalMoneyinCart}"></c:out>Ft</h1>
            <br>
            <table>
                <tr>
                    <th>
                        <form action="CustomerServlet" method="post"> 
                            <button type="submit" class="btn btn-success">TOVÁBB A FIZETÉSRE</button>
                            <input type="hidden" id="listProductBean" name="listProductBean" value="${listProductBean}">
                    </form>
                </th>
                <th>
                    <form action="ShoppingServlet" method="get"> 
                        <button type="submit" class="btn btn-primary">VISSZA A VÁSÁRLÁSHOZ</button>
                        <input type="hidden" id="listProductBean" name="listProductBean" value="${listProductBean}">
                    </form>
                </th>
                <th>      
                    <form action="CustomerServlet" method="post"> 
                            <button type="submit" class="btn btn-danger">BEVÁSÁRLÓLISTA TÖRLÉSE</button>
                            <input type="hidden" id="toDelete" name="toDelete" value="TRUE">
                    </form>
                </th>
        </tr>
    </table>
</body>
</html>

