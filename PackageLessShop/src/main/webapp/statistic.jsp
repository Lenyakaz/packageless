<%-- 
    Document   : statistic
    Created on : 2020.09.09., 11:08:12
    Author     : Otto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Kifogyó Termékek</title>
        <style>

            #locations {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
                color: white;
            }

            #locations td, #locations th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #locations tr:nth-child(even){background-color: #bfbfbf;}

            #locations tr:hover {background-color: #ddd;}

            #locations th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color:#a04646;
                color: white;
            }
            
           
            
            
            h1 {
            font-size:45px;
            color: #fff;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.5);
        }

        h2 {
            font-size:75px;
            color: #f08d3c;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.7);
        }

        p {
            font-size:20px;
            margin-top:0;
            margin-bottom:0;
            color: #fff;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.1);
        }


            bg { /* The image used */
                /* background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQmBzZMZtm0TGbEKnQwOoVPANmlme9LhZTT-amIUDvMTwGQUIoP"); */
                background-image: url("Images/wallpaper2you_109415.jpg");
                /* Full height */
                height: 100%; 

                /* Center and scale the image nicely */
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover; }
            body {
                background-image: url("Images/wallpaper-mania.com_High_resolution_wallpaper_background_ID_77701450778.jpg");
            }
            
        </style>
        <%@include file="WEB-INF/head.jsp" %>
        

    </head>
    <body>

        <%@include file="WEB-INF/menu.jsp" %>




        <h1>Statisztikák - lekérdezések</h1>   


        <h2>vásárlások</h2>

        <p><strong></strong></p>


        <table id="locations">
            <tr>
                <th> id </th>
                <th> date </th>
                <th> moneySpent - HUF </th>
                <th> location_id </th>



            </tr>






            <c:forEach items="${shoppingStat}" var="s">

                <tr>

                    <td><c:out value="${s.id}"></c:out></td>
                    <td><c:out value="${s.localDate}"></c:out></td>
                    <td><c:out value="${s.moneySpent}"></c:out></td>
                    <td><c:out value="${s.location_id}"></c:out></td>



                </c:forEach>



            </tr> 

        </table>

        <form action="StatisticServlet" method="POST">
            <div class="topnav">  

                <button type="submit" name="button" value="button1">Összes eladás</button>

                <label for="quantity">Keresés dátum szerint:</label>
                <input type="date" id="date" name="date">

                <input type="submit" value="Keres">

                <button type="submit" name="button" value="button2">Vásárlások 5000 Ft felett</button>


            </div> 


        </form>





    </body>
</html>














<br>
</body>
</html>

