<%-- 
    Document   : locations
    Created on : 2020.08.10., 15:59:21
    Author     : tbiro és ottika
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Kifogyó Termékek</title>
        <style>

            #locations {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
                color: white;
            }

            #locations td, #locations th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #locations tr:nth-child(even){background-color: #bfbfbf;}

            #locations tr:hover {background-color: #ddd;}

            #locations th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color:#a04646;
                color: white;
            }
            
              h1 {
            font-size:45px;
            color: #fff;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.5);
        }

        h2 {
            font-size:75px;
            color: #f08d3c;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.7);
        }

        p {
            font-size:20px;
            margin-top:0;
            margin-bottom:0;
            color: #fff;
            text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.1);
        }

           
         
             h3 {
                font-size:38px;
                color: #fff;
                text-shadow: 4px 4px 3px rgba(0, 0, 0, 0.5);
            }


            bg{ /* The image used */
                /* background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQmBzZMZtm0TGbEKnQwOoVPANmlme9LhZTT-amIUDvMTwGQUIoP"); */
                background-image: url("Images/wallpaper2you_109415.jpg");
                /* Full height */
                height: 100%; 

                /* Center and scale the image nicely */
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover; }
            body {
                background-image: url("Images/wallpaper-mania.com_High_resolution_wallpaper_background_ID_77701450778.jpg");
            }
            
        </style>
        <%@include file="WEB-INF/head.jsp" %>





    <body>

        <%@include file="WEB-INF/menu.jsp" %>
        
        <h1>Mobil üzletünk megállói és a vásárlás idöpontjai - itt jelentkezhet vásárlónak! </h1>
        <h2>Önnek meg kell adnia az adott kerületet ahol vásárolni szeretne - illetve a vásárlók számát (max 2 fő ) </h2>
       <form action="LocationServlet" method="post">
<!--            fenn előtte dopost volt!-->
        <table id="locations">
            <tr>
                <th> Kerület </th>
                <th> Cím </th>
                <th> Dátum </th>
                <th> Időpont </th>
                <th> Regisztráltak száma</th>
                
                
            </tr>
          
            <c:forEach items="${locationList}" var="p">
 
                <tr>

                    
                    <td><c:out value="${p.district}"></c:out></td>
                    <td><c:out value="${p.address}"></c:out></td>
                    
                    <td><c:out value="${p.shoppingDate}"></c:out></td>
                    <td><c:out value="${p.shoppingTime}"></c:out></td>
                    <td><c:out value="${p.wannaShop}"></c:out></td>
                                
                     </c:forEach>
                  
                    <td><label for="district">Kerület:</label></td>
                    <td><input type="number" id="district" name="district" value=1 min="1" max="23"></td>
                    <td><label for="quantity">Hány fő:</label></td>
                    <td><input type="number" id="quantity" name="quantity" value=0 min="0" max="2"></td>
                    <td> <input type="submit" value="Véglegesítem"></td>

                    </tr> 




            
            <!--            <form method="get" action="chooseLocation">
            <th colspan="1">
                <input type="submit" value="Jelentkezem erre a címre!" ${sessionScope.dept eq 'user.isAbleToChooseLocation' ? 'false' : 'disabled' }/>
            </th>  
            <th>
            ${sessionScope.dept}
        </th>
    </form>-->
        </table>
        </form>

        <br>
    </body>
</html>
