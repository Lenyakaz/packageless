<%-- 
    Document   : thanks
    Created on : 2020.09.10., 14:36:45
    Author     : tbiro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Csomagolásmentes Bolt</title>
        <link rel="StyleSheet" type="text/css" href="style.css">
        <%@include file="WEB-INF/head.jsp" %>
        <style>
            body {background-color: #cc6600;}
        </style>
    </head>
    <body>
        <div class="container">
            <%@include file="WEB-INF/menu.jsp" %>
            <div class="row">
                <h2>Nagyon köszönjük az együttműködést! Legyen szerencsénk legközelebb is!</h2>
            </div>
            <form action="index.jsp"> 
                <button type="submit" class="btn btn-warning">VISSZA A FŐOLDALRA</button>
                <input type="hidden" >
        </form>
        </div>

    </body>
</html>
