
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Register</title>
        <script>
            function validate()
            {
                var username = document.form.username.value;
                var email = document.form.email.value;
                var email = document.form.email.focus;
                var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
                var password = document.form.password.value;
                var conpassword = document.form.conpassword.value;

                if (email.value.match(mailformat))
                {
                    alert("Valid email address!");
                    document.form.email.focus();
                    return true;
                } else
                {
                    alert("You have entered an invalid email address!");
                    document.form.email.focus();
                    return false;
                }



                if (username === null || username === "")
                {
                    alert("A user name nem lehet �res");
                    return false;
                } else if (email === null || email === "")
                {
                    alert("Email nem lehet �res");


                    return false;
                } else if (password.length < 6)
                {
                    alert("Password legalabb 6 karakter hossz� legyen .");
                    return false;
                } else if (password !== conpassword)
                {
                    alert("a Passwordnek egyeznie kell !");
                    return false;
                }
            }
        </script> 
    </head>
    <body>
        <center><h2>Packageless shop registration</h2></center>
        <form name="form" action="RegisterServlet" method="get" onsubmit="return validate()">
            <table align="center">
                <tr>
                    <td>User_Name</td>
                    <td><input type="text" name="username" /></td>
                </tr>
                <td>Email</td>
                <td><input type="text" name="email" /></td>
                <tr>        
                    <td>Password</td>
                    <td><input type="Password" name="password" /></td>
                </tr>
                <tr>
                    <td>Confirm Password</td>
                    <td><input type="Password" name="conpassword" /></td>
                </tr>
                <tr>
                        <td><%=(request.getAttribute("errMessage") == null) ? ""
                 : request.getAttribute("errMessage")%></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Register">
                        <input  type="reset" value="Reset">

                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>

